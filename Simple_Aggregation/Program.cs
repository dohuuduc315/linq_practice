﻿using Simple_Aggregation;

var orders = new List<Order>
{
    new Order{ Id = 1, CustomerName = "Duc", TotalAmount = 50},
    new Order{ Id = 2, CustomerName = "specific customer", TotalAmount= 200},
    new Order{ Id = 3, CustomerName = "Vinh", TotalAmount = 170},
    new Order{ Id = 4, CustomerName = "specific customer", TotalAmount = 10},
    new Order{ Id = 5, CustomerName = "specific customer", TotalAmount = 250},
    new Order{ Id = 6, CustomerName = "Hung", TotalAmount = 90}
};

//----------Get the total number of all orders----------------

var totalNumber = orders.Count();
Console.WriteLine($"Total number of all orders: {totalNumber}");

//----------Get the total amount of all orders----------------

var totalAmount = orders.Sum(o => o.TotalAmount);
Console.WriteLine($"Total amount of all orders: {totalAmount}");

//----------Get the highest order amount---------------------

var highestAmount = orders.Max(o => o.TotalAmount);
Console.WriteLine($"Highest order amount: {highestAmount}");

//--------------Get the lowest order amount--------------------

var lowestAmount = orders.Min(o => o.TotalAmount);
Console.WriteLine($"Lowest order amount: {lowestAmount}");

//--------------Get the average order amount--------------------

var averageAmount = orders.Average(o => o.TotalAmount);
Console.WriteLine($"Average order amount: {averageAmount}");

//--------------Get the total number of orders for a specific customer-----------------

var totalNumberForCustomer = orders.Count(x => x.CustomerName == "specific customer");
Console.WriteLine($"Total number of orders for a specific customer: {totalNumberForCustomer}");

//---------------Get the total amount of all orders for a specific customer-------------

var totalAmountForCustomer = orders.Where(x => x.CustomerName == "specific customer")
                                   .Sum(x => x.TotalAmount);
Console.WriteLine($"Total amount of all orders for a specific customer: {totalAmountForCustomer}");

//-------------Get the highest order amount for a specific customer-----------------

var highestAmountForCustomer = orders.Where(x => x.CustomerName == "specific customer")
                                     .Max(x => x.TotalAmount);
Console.WriteLine($"Highest order amount for a specific customer: {highestAmountForCustomer}");

//-------------Get the highest order amount for a specific customer-----------------

var lowestAmountForCustomer = orders.Where(x => x.CustomerName == "specific customer")
                                     .Min(x => x.TotalAmount);
Console.WriteLine($"Lowest order amount for a specific customer: {lowestAmountForCustomer}");

//--------------Get the average order amount for a specific customer----------------

var averageAmountForCustomer = orders.Where(x => x.CustomerName == "specific customer")
                                     .Average(x => x.TotalAmount);
Console.WriteLine($"Average order amount for a specific customer: {averageAmountForCustomer}");