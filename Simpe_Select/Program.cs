﻿using Simpe_Select;

List<Person> people = new List<Person>()
{
    new Person() { Id = 1, FirstName = "John", LastName = "Doe", Age = 25 },
    new Person() { Id = 2, FirstName = "Jane", LastName = "Doe", Age = 30 },
    new Person() { Id = 3, FirstName = "Bob", LastName = "Smith", Age = 45 },
    new Person() { Id = 4, FirstName = "Alice", LastName = "Johnson", Age = 20 },
    new Person() { Id = 5, FirstName = "Tom", LastName = "Lee", Age = 50 }
};

//----------Select the first name of all people----------------

var getFirstName = people.Select(p => p.FirstName);
Console.WriteLine("First name of all people");
foreach (var item in getFirstName)
{
    Console.WriteLine(item);
}

var getFirstNameByQuery = from p in people // Query syntax
                          select p.FirstName;
Console.WriteLine("-------------------------------------");
Console.WriteLine("First name of all people(Query)");
foreach (var item in getFirstNameByQuery)
{
    Console.WriteLine(item);
}

//------------Select the full name (first name and last name) of all people------------------

var getFullName = people.Select(x => new { FirstName = x.FirstName, LastName = x.LastName });
Console.WriteLine("-------------------------------------");
Console.WriteLine("Full name of all people");
foreach (var item in getFullName)
{
    Console.WriteLine($"{item.FirstName} {item.LastName}");
}

var getFullNameByQuery = from p in people // Query syntax
                         select new { FirstName = p.FirstName, LastName = p.LastName };
Console.WriteLine("-------------------------------------");
Console.WriteLine("Full name of all people(Query)");
foreach (var item in getFullNameByQuery)
{
    Console.WriteLine($"{item.FirstName} {item.LastName}");
}

//-------------------Select the first name and age of all people--------------------

var getFirstNameAndAge = people.Select(x => new { FirstName = x.FirstName, Age = x.Age });
Console.WriteLine("-------------------------------------");
Console.WriteLine("First name and age of all people");
foreach (var item in getFirstNameAndAge)
{
    Console.WriteLine($"{item.FirstName} - {item.Age}");
}
var getFirstNameAndAgeByQuery = from p in people
                                select new { FirstName = p.FirstName, Age = p.Age };
Console.WriteLine("-------------------------------------");
Console.WriteLine("First name and age of all people(Query)");
foreach (var item in getFirstNameAndAgeByQuery)
{
    Console.WriteLine($"{item.FirstName} - {item.Age}");
}

//-----------------Select the length of the first name of all people---------------------

var getLength = people.Select(x => x.FirstName.Length);
Console.WriteLine("-------------------------------------");
Console.WriteLine("length of the first name of all people");
foreach (var item in getLength)
{
    Console.WriteLine(item);
}
var getLengthByQuery = from p in people
                       select p.FirstName.Length;
Console.WriteLine("-------------------------------------");
Console.WriteLine("length of the first name of all people(Query)");
foreach (var item in getLengthByQuery)
{
    Console.WriteLine(item);
}

//-------------------Select the last name in uppercase of all people--------------------------

var uppercaseLastName = people.Select(x => x.LastName.ToUpper());
Console.WriteLine("-------------------------------------");
Console.WriteLine("Lastame in uppercase of all people");
foreach (var item in uppercaseLastName)
{
    Console.WriteLine(item);
}
//-------------------Select the age divided by 2 of all people-----------------

var ageDividedBy2 = people.Select(x => x.Age / 2);
Console.WriteLine("-------------------------------------");
foreach (var item in ageDividedBy2)
{
    Console.WriteLine($"Age divided by 2 of all people: {item}");
}

//------------------Select the first and last name of all people as a single string, separated by a space---------------

var separated = people.Select(x => x.FirstName + " " + x.LastName);
Console.WriteLine("-------------------------------------");
Console.WriteLine("First and last name of all people as a single string, separated by a space");
foreach (var item in separated)
{
    Console.WriteLine(item);
}

//-----------------Select the first name and age of all people where the age is greater than 30.(anonymouse class)---------------

Console.WriteLine("-------------------------------------");
var getAgeGreater = people.Where(x => x.Age >= 30)
                          .Select(x => new { x.FirstName, x.Age });
Console.WriteLine("First name and age of all people where the age is greater than 30");
foreach (var item in getAgeGreater)
{
    Console.WriteLine($"{item.FirstName} - {item.Age}");
}

//---------------Select the first name and age of all people, and sort the result by age in ascending order------------------

Console.WriteLine("-------------------------------------");
var sortByAge = people.Select(x => new { x.FirstName, x.Age})
                      .OrderBy(x => x.Age);
Console.WriteLine("First name and age of all people, and sort the result by age in ascending order");
foreach (var item in sortByAge)
{
    Console.WriteLine($"{item.FirstName} - {item.Age}");
}