﻿namespace Simple_Combine
{
    public class Employee
    {
        public int Id { get; set; }
        public String Name { get; set; }
        public int Age { get; set; }
        public decimal Salary { get; set; }
    }
}
