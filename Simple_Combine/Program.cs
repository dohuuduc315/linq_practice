﻿using Simple_Combine;
using System.Security.Cryptography.X509Certificates;

var employees = new List<Employee>
{
    new Employee {Id = 1, Name = "Duc", Age = 41, Salary = 20_000},
    new Employee {Id = 2, Name = "Hung", Age = 24, Salary = 60_000},
    new Employee {Id = 3, Name = "Dang", Age = 19, Salary = 45_000},
    new Employee {Id = 4, Name = "Jack", Age = 39, Salary = 100_000},
    new Employee {Id = 5, Name = "Vu", Age = 50, Salary = 150_500}
};

//---------Get the name and age of all employees whose age is greater than 30, sorted by name in ascending order--------------

var sortedNameWithAge = employees.Where(x => x.Age > 30)
                                 .OrderBy(x => x.Name)
                                 .Select(x => new { Name = x.Name, Age = x.Age });
foreach (var item in sortedNameWithAge)
{
    Console.WriteLine($"{item.Name} - {item.Age}");
}
var sortedNameWithAgeByQuery = from emp in employees // Query syntax
                               where emp.Age > 30
                               orderby emp.Name
                               select new { Name = emp.Name, Age = emp.Age };
foreach (var item in sortedNameWithAge)
{
    Console.WriteLine($"Query syntax: {item.Name} - {item.Age}");
}

//------------Get the total salary of all employees whose age is between 25 and 35---------------------

var getTotalSalary = employees.Where(x => x.Age >= 25 && x.Age <= 35)
                              .Sum(x => x.Salary);
Console.WriteLine($"Total salary of all employees whose age is between 25 and 35: {getTotalSalary}");

//------------Get the name and salary of the five highest-paid employees, sorted by salary in descending order-------------

var getFiveHighestPaidEmps = employees.OrderByDescending(x => x.Salary)
                                      .Take(5)
                                      .Select(x => new { Name = x.Name, Salary = x.Salary });
foreach (var item in getFiveHighestPaidEmps)
{
    Console.WriteLine($"Five highest-paid employees: {item.Name} - {item.Salary}");
}

//------------Get the average age of all employees whose name contains the letter "a"--------------------

var contain = employees.Where(x => x.Name.Contains("a"))
                       .Average(x => x.Age);
Console.WriteLine($"Average age of all employees whose name contains the letter a: {contain}");

//------Get the name and salary of the three lowest-paid employees who are older than 40, sorted by salary in ascending order--------

var getThreeLowestPaidEmps = employees.Where(x => x.Age > 40)
                                      .OrderBy(x => x.Salary)
                                      .Take(3)
                                      .Select(x => new { Name = x.Name, Salary = x.Salary });
foreach (var item in getThreeLowestPaidEmps)
{
    Console.WriteLine($"Three lowest-paid employees who are older than 40: {item.Name} - {item.Salary}");
}

//--------Get the name and age of the oldest employee---------------

var getOldestEmp = employees.OrderByDescending(x => x.Age)
                            .Select(x => new { Name = x.Name, Age = x.Age})
                            .First();
Console.WriteLine($"Oldest employee: {getOldestEmp.Name} - {getOldestEmp.Age}");

//--------Get the name and age of the youngest employee whose salary is greater than $50,000---------

var getYoungestEmp = employees.OrderBy(x => x.Age)
                              .Where(x => x.Salary > 50_000)
                              .Select(x => new { Name = x.Name, Age = x.Age})
                              .First();
Console.WriteLine($"Youngest employee whose salary is greater than $50,000: {getYoungestEmp.Name} - {getYoungestEmp.Age}");

//------------Get the total number of employees whose salary is greater than $70,000----------------

var getTotalNumberEmp = employees.Where(x => x.Salary > 70_000)
                                 .Count();
Console.WriteLine($"Total number of employees whose salary is greater than $70,000: {getTotalNumberEmp}");

//-----------Get the average salary of all employees who are older than 35 and whose name starts with the letter "J"----------

try
{
    var getAverageSalaryEmp = employees.Where(x => x.Age > 35 && x.Name.StartsWith("J"))
                                   .Average(x => x.Salary);
    Console.WriteLine($"Average salary of all employees who are older than 35 and whose name starts with the letter J: {getAverageSalaryEmp}");
}
catch (Exception)
{
    throw new Exception("Not Found");
}

//-----------Get the name and age of the employee with the highest salary-------------------

var getHighestSalary = employees.OrderByDescending(x => x.Salary)
                                .Select(x => new { Name = x.Name, Age = x.Age })
                                .First();
Console.WriteLine($"Name and age of the employee with the highest salary: {getHighestSalary.Name} - {getHighestSalary.Age}");