﻿using Simple_Where;

List<Person> people = new List<Person>()
{
    new Person { Id = 1, FirstName = "John", LastName = "Doe", Age = 25 },
    new Person { Id = 2, FirstName = "Jane", LastName = "Doe", Age = 30 },
    new Person { Id = 3, FirstName = "Bob", LastName = "Smith", Age = 45 },
    new Person { Id = 4, FirstName = "Alice", LastName = "Jon", Age = 20 },
    new Person { Id = 5, FirstName = "Tom", LastName = "Lee", Age = 50 }
};

//-------------------Select all people who are younger than 30 years old------------------------
Console.WriteLine("Select all people who are younger than 30 years old");
var selectYoungerPeople = people.Where(p => p.Age < 30)
                                .Select(x => x.FirstName + " " + x.LastName);
foreach (var item in selectYoungerPeople)
{
    Console.WriteLine(item);
}

Console.WriteLine("---------------------------------------");
var selectYoungerPeopleByQuery = from p in people //Query syntax
                                 where p.Age < 30
                                 select p.FirstName + " " + p.LastName;
foreach (var item in selectYoungerPeopleByQuery)
{
    Console.WriteLine(item);
}

//-----------------Select all people whose first name starts with "J"----------------------

Console.WriteLine("---------------------------------------");
Console.WriteLine("Select all people whose first name starts with J");
var selectStartWith = people.Where(p => p.FirstName.StartsWith("J"))
                            .Select(p => p.FirstName + " " + p.LastName);
foreach (var item in selectStartWith)
{
    Console.WriteLine(item);
}

//----------------------Select all people whose last name is "Doe"----------------------------

Console.WriteLine("---------------------------------------");
Console.WriteLine("Select all people whose last name is Doe");
var selectLastName = people.Where(p => p.LastName.Contains("Doe"))
                           .Select(p => p.FirstName + " " + p.LastName);
foreach (var item in selectLastName)
{
    Console.WriteLine(item);
}

//----------------------Select all people whose first name contains the letter "a"-----------------------

Console.WriteLine("---------------------------------------");
Console.WriteLine("Select all people whose first name contains the letter a");
var getFirstNameContain = people.Where(p => p.FirstName.Contains("a"))
                                .Select(p => p.FirstName);
foreach (var item in getFirstNameContain)
{
    Console.WriteLine(item);
}

//------------------Select all people whose last name contains the letter "i" and age is greater than or equal to 40----------------

Console.WriteLine("---------------------------------------");
Console.WriteLine("Select all people whose last name contains the letter i and age is greater than or equal to 40");
var getLastNameContainAgeGreater = people.Where(p => p.LastName.Contains("i") && p.Age >= 40)
                                         .Select(p => p.LastName + " " + p.FirstName);
foreach (var item in getLastNameContainAgeGreater)
{
    Console.WriteLine(item);
}

//-------------------Select all people whose first name is longer than 4 characters------------------------

Console.WriteLine("---------------------------------------");
Console.WriteLine("Select all people whose first name is longer than 4 characters");
var getFirstNameLonger = people.Where(p => p.FirstName.Length > 4)
                               .Select(p => p.FirstName);
foreach (var item in getFirstNameLonger)
{
    Console.WriteLine(item);
}

//-------------------Select all people whose last name is not "Johnson"-------------------------

Console.WriteLine("---------------------------------------");
Console.WriteLine("Select all people whose last name is not Johnson");
var getLastName = people.Where(p => p.LastName != "Jon")
                        .Select(p => p.LastName);
foreach (var item in getLastName)
{
    Console.WriteLine(item);
}

//------------------Select all people whose age is between 30 and 50 (inclusive)-----------------

Console.WriteLine("---------------------------------------");
Console.WriteLine("Select all people whose age is between 30 and 50");
var getAgeBetween = people.Where(p => p.Age > 30 && p.Age < 50)
                          .Select(p => new { p.FirstName, p.LastName, p.Age });
foreach (var item in getAgeBetween)
{
    Console.WriteLine($"{item.FirstName} {item.LastName} - {item.Age}");
}

//-----------------Select all people whose first name and last name are not the same------------------

Console.WriteLine("---------------------------------------");
Console.WriteLine("Select all people whose first name and last name are not the same");
var getFirstAndLastNotSame = people.Where(p => p.FirstName != p.LastName)
                                   .Select(p => new { p.FirstName, p.LastName });
foreach (var item in getFirstAndLastNotSame)
{
    Console.WriteLine($"{item.FirstName} - {item.LastName}");
}

//--------------Select all people whose last name starts with "S" and age is greater than 30---------------

Console.WriteLine("---------------------------------------");
Console.WriteLine("Select all people whose last name starts with S and age is greater than 30");
var getLastNameStartWith = people.Where(p => p.LastName.StartsWith("S") && p.Age > 30)
                                 .Select(p => new { p.LastName, p.Age });
foreach (var item in getLastNameStartWith)
{
    Console.WriteLine($"{item.LastName} - {item.Age}");
}