﻿
using Sort_Data;
using System.Security.Cryptography.X509Certificates;

var employees = new List<Employee>
{
    new Employee {Id = 1, Name = "Duc", Age = 21, Salary = 2_000_000},
    new Employee {Id = 2, Name = "Hung", Age = 24, Salary = 1_000_000},
    new Employee {Id = 3, Name = "Dang", Age = 19, Salary = 4_500_000},
    new Employee {Id = 4, Name = "Son", Age = 22, Salary = 5_000_000},
    new Employee {Id = 5, Name = "Vu", Age = 25, Salary = 1_500_000}
};

//------Sort employees by name in ascending order--------

var sortEmployeeName = employees.OrderBy(x => x.Name) //method syntax
                                .Select(x => x.Name);
foreach (var item in sortEmployeeName)
{
    Console.WriteLine($"Sort employees by name in ascending order: {item}");
}
var sortEmpNameByQuery = from emp in employees // query syntax
                         orderby emp.Name
                         select emp.Name;
foreach (var item in sortEmpNameByQuery)
{
    Console.WriteLine($"Sort employees by name in ascending order(Query): {item}");
}

//-------------Sort employees by age in descending order.---------------

var sortEmployeeAge = employees.OrderByDescending(x => x.Age)
                               .Select(x => new { Name = x.Name, Age = x.Age});
foreach (var item in sortEmployeeAge)
{
    Console.WriteLine($"Sort employees by age in descending order: {item.Name} - {item.Age}");
}

var sortEmpAgeByQuery = from emp in employees //query syntax
                        orderby emp.Age
                        select new {Name = emp.Name, Age = emp.Age};

foreach (var item in sortEmpAgeByQuery)
{
    Console.WriteLine($"Sort employees by age in descending order(Query): {item.Name} - {item.Age}");
}

//-------------Sort employees by salary in ascending order.-----------------
var sortEmployeeSalary = employees.OrderBy(x => x.Salary)
                                  .Select(x => x.Name);
foreach (var item in sortEmployeeSalary)
{
    Console.WriteLine($"Sort employees by salary in ascending order: {item}");
}

var sortEmpSalaryByQuery = from emp in employees
                           orderby emp.Salary
                           select emp.Name;
foreach (var item in sortEmpSalaryByQuery)
{
    Console.WriteLine($"Sort employees by salary in ascending order(Query): {item}");
}

//--------------Sort employees by salary in descending order, then by name in ascending order---------------

var sortEmpSalaryAndName = employees.OrderByDescending(x => x.Salary)
                                    .OrderBy(x => x.Name)
                                    .Select(x => new { Name = x.Name, Salary = x.Salary });
foreach (var item in sortEmpSalaryAndName)
{
    Console.WriteLine($"Sort employees by salary in descending order, then by name in ascending order: {item.Name} - {item.Salary}");
}

var sortEmpSalaryAndNameByQuery = from emp in employees
                                  orderby emp.Salary descending
                                  orderby emp.Name
                                  select new { Name = emp.Name, Salary = emp.Salary };
foreach (var item in sortEmpSalaryAndNameByQuery)
{
    Console.WriteLine($"Sort employees by salary in descending order, then by name in ascending order(Query): {item.Name} - {item.Salary}");
}

//------------Sort employees by name in descending order, then by age in ascending order----------------

var sortEmpNameAndAge = employees.OrderByDescending(x => x.Name)
                                 .OrderBy(x => x.Age)
                                 .Select(x => new { Name = x.Name, Age = x.Age });
foreach (var item in sortEmpNameAndAge)
{
    Console.WriteLine($"Sort employees by name in descending order, then by age in ascending order: {item.Name} - {item.Age}");
}
var sortEmpNameAndAgeByQuery = from emp in employees
                               orderby emp.Name descending
                               orderby emp.Age
                               select new { Name = emp.Name, Age = emp.Age };
foreach (var item in sortEmpNameAndAgeByQuery)
{
    Console.WriteLine($"Sort employees by name in descending order, then by age in ascending order(Query): {item.Name} - {item.Age}");
}

//---------------Sort employees by name in ascending order, then by age in descending order, then by salary in ascending order---------

var sortEmpNameAgeSalary = employees.OrderBy(x => x.Name)
                                    .OrderByDescending(x => x.Age)
                                    .OrderBy(x => x.Salary)
                                    .Select(x => new { Name = x.Name, Age = x.Age, Salary = x.Salary });
foreach (var item in sortEmpNameAgeSalary)
{
    Console.WriteLine($"Sort employees by name, salary in ascending order, by age in descending order: {item.Name} - {item.Age}, {item.Salary}");
}
var sortEmpNameAgeSalaryByQuery = from emp in employees
                                  orderby emp.Name
                                  orderby emp.Age descending
                                  orderby emp.Salary
                                  select new { Name = emp.Name, Age = emp.Age, Salary = emp.Salary };
foreach (var item in sortEmpNameAgeSalaryByQuery)
{
    Console.WriteLine($"Sort employees by name, salary in ascending order, by age in descending order(Query): {item.Name} - {item.Age}, {item.Salary}");
}

//--------Sort employees by salary in descending order, then by age in ascending order, then by name in descending order------------

var sortEmpBySalaryNameAge = employees.OrderByDescending(x => x.Salary)
                                      .OrderBy(x => x.Age)
                                      .OrderByDescending(x => x.Name)
                                      .Select(x => new { Salary = x.Salary, Age = x.Age, Name = x.Name });
foreach (var item in sortEmpBySalaryNameAge)
{
    Console.WriteLine($"{item.Salary} - {item.Age} - {item.Name}");
}
var sortEmpBySalaryNameAgeByQuery = from emp in employees
                                    orderby emp.Salary descending
                                    orderby emp.Age
                                    orderby emp.Name descending
                                    select new { Salary = emp.Salary, Age = emp.Age, Name = emp.Name };
foreach (var item in sortEmpBySalaryNameAgeByQuery)
{
    Console.WriteLine($"Query Syntax: {item.Salary} - {item.Age} - {item.Name}");
}

//---------Sort employees by name in descending order, then by salary in ascending order, then by age in descending order------------

var sortEmpByNameSalaryAge = employees.OrderByDescending(x => x.Name)
                                      .OrderBy(x => x.Salary)
                                      .OrderByDescending(x => x.Age)
                                      .Select(x => new { Name = x.Name, Salary = x.Salary, Age = x.Age });
foreach (var item in sortEmpByNameSalaryAge)
{
    Console.WriteLine($"{item.Name} - {item.Salary} - {item.Age}");
}
var sortEmpByNameSalaryAgeByQuery = from emp in employees
                                    orderby emp.Name descending
                                    orderby emp.Salary
                                    orderby emp.Age descending
                                    select new { Name = emp.Name, Salary = emp.Salary, Age = emp.Age };
foreach (var item in sortEmpByNameSalaryAge)
{
    Console.WriteLine($"Query syntax: {item.Name} - {item.Salary} - {item.Age}");
}

//------------Sort employees by age in descending order, then by name in ascending order, then by salary in descending order------------
var sortEmpByAgeNameSalary = employees.OrderByDescending(x => x.Age)
                                      .OrderBy(x => x.Name)
                                      .OrderByDescending(x => x.Salary)
                                      .Select(x => new { Age = x.Age, Name = x.Name, Salary = x.Salary });
foreach (var item in sortEmpByAgeNameSalary)
{
    Console.WriteLine($"{item.Age} - {item.Name} - {item.Salary}");
}
var sortEmpByAgeNameSalaryByQuery = from emp in employees
                                    orderby emp.Age descending
                                    orderby emp.Name
                                    orderby emp.Salary descending
                                    select new { Age = emp.Age, Name = emp.Name, Salary = emp.Salary };
foreach (var item in sortEmpByAgeNameSalaryByQuery)
{
    Console.WriteLine($"Query syntax: {item.Age} - {item.Name} - {item.Salary}");
}